package Controler;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JButton;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.plaf.basic.BasicOptionPaneUI.ButtonActionListener;

import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;


import codigoNegocio.GrafoArgentina;
import interfazGrafica.Arista;
import interfazGrafica.Mapa;
import interfazGrafica.Menu;
import interfazGrafica.Vertice;

public class Controler  {

	private JTextField cantRegionesInput;
	private ArrayList<JTextField> inputsPesos;
	private ArrayList<JTextArea> texts;
	private JButton calcularRegionesButton;
	private JButton mostrarGrafoCompletoButton;	
	private Mapa mapa;
	private ArrayList<AristaTO> aristasTOCompletas;
	private ArrayList<AristaTO> aristasTORegiones;
	private GrafoArgentina grafoArgentina;
	
	public Controler(Mapa mapa, Menu menu) {
		
		this.cantRegionesInput = menu.getCantRegionesInput();
		this.inputsPesos = menu.getInputsPesos();
		this.texts = menu.getTexts();
		this.calcularRegionesButton = menu.getCalcularRegionesButton();
		this.mostrarGrafoCompletoButton = menu.getMostrarGrafoCompletoButton();
		this.mapa = mapa;
		this.aristasTOCompletas = new ArrayList<AristaTO>();
		
		this.mostrarGrafoCompletoButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{		
					agregarAristas();
					enviarAlNegocio(aristasTOCompletas);					
			}	
		});
		this.calcularRegionesButton.addActionListener(new ActionListener(){
			@Override
			public void actionPerformed(ActionEvent e) 
			{		
					separarRegiones();		
				
			}	
		});	
	}
	
	public void agregarAristas() {
		for(int i = 0; i<inputsPesos.size(); i++) {
			int peso = 1000;
			if(!inputsPesos.get(i).getText().equals(""))
				peso = Integer.parseInt(inputsPesos.get(i).getText());
			
			mapa.getAristas().get(i).setPeso(peso);
		}
		generarTO(mapa.getAristas());
	}
	
	public void generarTO(ArrayList<Arista> aristas) {
		
		this.aristasTOCompletas.clear();
		
		for(int i = 0; i<aristas.size(); i++) {
			this.aristasTOCompletas.add(new AristaTO(aristas.get(i).getPeso(),aristas.get(i).getVertice1().getNombre(),aristas.get(i).getVertice2().getNombre()));
		}
	}
	public void enviarAlNegocio(ArrayList<AristaTO> aristas) {
		
		this.grafoArgentina = new GrafoArgentina(aristas);
		
		actualizarAristas(this.aristasTOCompletas);
	}
	
	public void separarRegiones() {
		
		agregarAristas();
		
		this.grafoArgentina = new GrafoArgentina(this.aristasTOCompletas);
		
		this.grafoArgentina.separarRegionesAGM(Integer.parseInt(this.cantRegionesInput.getText()));
		
		this.aristasTORegiones = this.grafoArgentina.getAristasTO();
		
		actualizarAristas(this.aristasTORegiones);
	}

	public void dibujarAristas(ArrayList<AristaTO> aristasTO ) {
		
		String provincia1;
		String provincia2;
		
		Vertice vertice1;
		Vertice vertice2;
		
		MapPolygon mapPolygon;
		
		for(AristaTO arista : aristasTO) {
			provincia1 = arista.getVertice1();
			provincia2 = arista.getVertice2();
			
			vertice1 = mapa.getVertice(provincia1);
			vertice2 = mapa.getVertice(provincia2);
			
			mapPolygon = new MapPolygonImpl(vertice1.getCoordenada(), vertice2.getCoordenada(), vertice1.getCoordenada());
			
			mapa.addMapPolygon(mapPolygon);
		}
	}
	
	public void actualizarAristas(ArrayList<AristaTO> aristasTO) {
		
		List<MapPolygon> listaAristas = mapa.getMapPolygonList();
		
		listaAristas.clear();
		
		dibujarAristas(aristasTO);
		
	}
	
	
}




