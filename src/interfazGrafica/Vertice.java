package interfazGrafica;

import org.openstreetmap.gui.jmapviewer.Coordinate;

public class Vertice {

	private Coordinate coordenada;
	private String nombre;
	
	public Vertice (Coordinate coordenada, String nombre) {
		
		this.coordenada= coordenada;
		this.nombre = nombre;
		
	}

	public Coordinate getCoordenada() {
		return coordenada;
	}

	public void setCoordenada(Coordinate coordenada) {
		this.coordenada = coordenada;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	@Override
	public String toString() {
		return "Vertice [coordenada=" + coordenada + ", nombre=" + nombre + "]";
	}
	
	
	
}
