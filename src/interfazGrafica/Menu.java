package interfazGrafica;

import java.awt.Font;
import java.util.ArrayList;

import javax.swing.JButton;
import javax.swing.JEditorPane;
import javax.swing.JPanel;
import javax.swing.JTextArea;
import javax.swing.JTextField;

import Controler.Controler;

@SuppressWarnings("serial")
public class Menu extends JPanel{
	private JTextField tierradelFuegoSantaCruzInput;
	private JTextField santaCruzChubutInput;
	private JTextField rioNegroNeuquenInput;
	private JTextField chubutRioNegroInput;
	private JTextField rioNegroMendozaInput;
	private JTextField rioNegroLaPampaInput;
	private JTextField neuquenLaPampaInput;
	private JTextField rioNegroBsAsInput;
	private JTextField laPampaBsAsInput;
	private JTextField neuquenMendozaInput;
	private JTextField laPampaSanLuisInput;
	private JTextField laPampaMendozaInput;
	private JTextField bsAsCordobaInput;
	private JTextField laPampaCordobaInput;
	private JTextField bsAsSantaFeInput;
	private JTextField bsAsCABAInput;
	private JTextField mendozaSanLuisInput;
	private JTextField bsAsEntreRiosInput;
	private JTextField sanLuisCordobaInput;
	private JTextField mendozaSanJuanInput;
	private JTextField sanLuisLaRiojaInput;
	private JTextField sanLuisSanJuanInput;
	private JTextField cbaStgoDelEsteroInput;
	private JTextField cordobaSantaFeInput;
	private JTextField cbaCatamarcaInput;
	private JTextField cbaLaRiojaInput;
	private JTextField santaFeStgoDelEsteroInput;
	private JTextField santaFeEntreRiosInput;
	private JTextField santaFeChacoInput;
	private JTextField santaFeCorrientesInput;
	private JTextField sanJuanLaRiojaInput;
	private JTextField entreRiosCorrientesInput;
	private JTextField catamarcaStgoDelEsteroInput;
	private JTextField laRiojaCatamarcaInput;
	private JTextField catamarcaSaltaInput;
	private JTextField catamarcaTucumanInput;
	private JTextField tucumanSaltaInput;
	private JTextField tucumanStgoDelEsteroInput;
	private JTextField stgoDelEsteroChacoInput;
	private JTextField stgoDelEsteroSaltaInput;
	private JTextField chacoFormosaInput;
	private JTextField chacoSaltaInput;
	private JTextField corrientesMisionesInput;
	private JTextField chacoCorrientesInput;
	private JTextField saltaFormosaInput;
	private JTextField saltaJujuyInput;	
	private JTextField cantRegionesInput;
	private JTextArea tierraDelFuegoSantaCruzText;
	private JTextArea santaCruzChubutText;
	private JTextArea chubutRionegroText;
	private JTextArea rioNegroNeuquenText;
	private JTextArea rioNegroLaPampaText;
	private JTextArea rioNegroMendozaText;
	private JTextArea rioNegroBsAsText;
	private JTextArea meuquenLaPampaText;
	private JTextArea neuquenMendozaText;
	private JTextArea laPampaBsAsText;
	private JTextArea laPampaMendozaText;
	private JTextArea laPampaSanLuisText;
	private JTextArea laPampaCordobaText;
	private JTextArea bsAsCordobaText;
	private JTextArea bsAsCABAText;
	private JTextArea bsAsSantaFeText;
	private JTextArea bsAsEntreRiosText;
	private JTextArea mendozaSanLuisText;
	private JTextArea mendozaSanJuanText;
	private JTextArea sanLuisCordobaText;
	private JTextArea sanLuisSanJuanText;
	private JTextArea sanLuisLaRiojaText;
	private JTextArea cordobaSantaFeText;
	private JTextArea cbaStgodelEsteroText;
	private JTextArea cordobaLaRiojaText;
	private JTextArea cordobaCatamarcaText;
	private JTextArea santaFeEntreRiosText;
	private JTextArea santaFeStgodelEsteroText;
	private JTextArea santaFeCorrientesText;
	private JTextArea santaFeChacoText;
	private JTextArea entreRiosCorrientesText;
	private JTextArea sanJuanLaRiojaText;
	private JTextArea laRiojaCatamarcaText;
	private JTextArea catamarcaStgodelEsteroText;
	private JTextArea catamarcaTucumanText;
	private JTextArea catamarcaSaltaText;
	private JTextArea tucumanStgodelEsteroText;
	private JTextArea tucumanSaltaText;
	private JTextArea stgodelEsteroSaltaText;
	private JTextArea stgodelEsteroChacoText;
	private JTextArea chacoSaltaText;
	private JTextArea chacoFormosaText;
	private JTextArea chacoCorrientesText;
	private JTextArea corrientesMisionesText;
	private JTextArea saltaJujuyText;
	private JTextArea saltaFormosaText;
	private JTextArea regionesASepararText; 
	private JEditorPane titulo;
	private JButton calcularRegionesButton;
	private JButton mostrarGrafoCompletoButton;
	private Controler Controller;
	private ArrayList<JTextField> inputs;
	private ArrayList<JTextArea> texts;
	private Mapa mapa;
	
	
	public Menu(Mapa mapa) {
		this.mapa = mapa;
		this.inputs = new ArrayList<JTextField>();
		this.texts = new ArrayList<JTextArea>();
		configurarMenu();
		inicializarTitulo();
		inicializarInputs();	
		inicializarTextFields();
		inicializarBotones();
		this.Controller = new Controler(this.mapa, this);		
	}
    public void configurarMenu() {
		setBounds(0,0,461,1011);		
		setLayout(null);
    }
	public void inicializarInputs() {

		this.tierradelFuegoSantaCruzInput = new JTextField();
		this.tierradelFuegoSantaCruzInput.setBounds(135, 32, 31, 20);	    
		this.tierradelFuegoSantaCruzInput.setColumns(10);
		add(this.tierradelFuegoSantaCruzInput);
		this.inputs.add(tierradelFuegoSantaCruzInput);
		generarArista("Tierra Del Fuego", "Santa Cruz");
		
		
		this.santaCruzChubutInput = new JTextField();
		this.santaCruzChubutInput.setBounds(364, 32, 31, 20);		
		this.santaCruzChubutInput.setColumns(10);
		add(this.santaCruzChubutInput);
		this.inputs.add(santaCruzChubutInput);
		generarArista("Santa Cruz", "Chubut");
		
		
		this.rioNegroNeuquenInput = new JTextField();
		this.rioNegroNeuquenInput.setColumns(10);
		this.rioNegroNeuquenInput.setBounds(364, 60, 31, 20);
		add(this.rioNegroNeuquenInput);
		this.inputs.add(rioNegroNeuquenInput);
		generarArista("Rio Negro", "Neuquen");
		
		this.chubutRioNegroInput = new JTextField();
		this.chubutRioNegroInput.setColumns(10);
		this.chubutRioNegroInput.setBounds(135, 60, 31, 20);
		add(this.chubutRioNegroInput);
		this.inputs.add(chubutRioNegroInput);
		generarArista("Chubut", "Rio Negro");
		
		this.rioNegroMendozaInput = new JTextField();
		this.rioNegroMendozaInput.setColumns(10);
		this.rioNegroMendozaInput.setBounds(364, 91, 31, 20);
		add(this.rioNegroMendozaInput);
		this.inputs.add(rioNegroMendozaInput);
		generarArista("Rio Negro", "Mendoza");
		
		this.rioNegroLaPampaInput = new JTextField();
		this.rioNegroLaPampaInput.setColumns(10);
		this.rioNegroLaPampaInput.setBounds(135, 91, 31, 20);
		add(this.rioNegroLaPampaInput);
		this.inputs.add(rioNegroLaPampaInput);
		generarArista("Rio Negro", "La Pampa");
		
		this.neuquenLaPampaInput = new JTextField();
		this.neuquenLaPampaInput.setColumns(10);
		this.neuquenLaPampaInput.setBounds(364, 122, 31, 20);
		add(this.neuquenLaPampaInput);
		this.inputs.add(neuquenLaPampaInput);
		generarArista("Neuquen", "La Pampa");
		
		this.rioNegroBsAsInput = new JTextField();
		this.rioNegroBsAsInput.setColumns(10);
		this.rioNegroBsAsInput.setBounds(135, 122, 31, 20);
		add(this.rioNegroBsAsInput);
		this.inputs.add(rioNegroBsAsInput);
		generarArista("Rio Negro", "Buenos Aires");
		
		this.laPampaBsAsInput = new JTextField();
		this.laPampaBsAsInput.setColumns(10);
		this.laPampaBsAsInput.setBounds(364, 153, 31, 20);
		add(this.laPampaBsAsInput);
		this.inputs.add(laPampaBsAsInput);
		generarArista("La Pampa", "Buenos Aires");
		
		this.neuquenMendozaInput = new JTextField();
		this.neuquenMendozaInput.setColumns(10);
		this.neuquenMendozaInput.setBounds(135, 153, 31, 20);
		add(this.neuquenMendozaInput);
		this.inputs.add(neuquenMendozaInput);
		generarArista("Neuquen", "Mendoza");
		
		this.laPampaSanLuisInput = new JTextField();
		this.laPampaSanLuisInput.setColumns(10);
		this.laPampaSanLuisInput.setBounds(364, 184, 31, 20);
		add(this.laPampaSanLuisInput);
		this.inputs.add(laPampaSanLuisInput);
		generarArista("La Pampa", "San Luis");
		
		this.laPampaMendozaInput = new JTextField();
		this.laPampaMendozaInput.setColumns(10);
		this.laPampaMendozaInput.setBounds(135, 184, 31, 20);
		add(this.laPampaMendozaInput);
		this.inputs.add(laPampaMendozaInput);
		generarArista("La Pampa", "Mendoza");
		
		this.bsAsCordobaInput = new JTextField();
		this.bsAsCordobaInput.setColumns(10);
		this.bsAsCordobaInput.setBounds(364, 215, 31, 20);
		add(this.bsAsCordobaInput);
		this.inputs.add(bsAsCordobaInput);
		generarArista("Buenos Aires", "Cordoba");
		
		this.laPampaCordobaInput = new JTextField();
		this.laPampaCordobaInput.setColumns(10);
		this.laPampaCordobaInput.setBounds(135, 215, 31, 20);
		add(this.laPampaCordobaInput);
		this.inputs.add(laPampaCordobaInput);
		generarArista("La Pampa", "Cordoba");
		
		this.bsAsSantaFeInput = new JTextField();
		this.bsAsSantaFeInput.setColumns(10);
		this.bsAsSantaFeInput.setBounds(364, 246, 31, 20);
		add(this.bsAsSantaFeInput);
		this.inputs.add(bsAsSantaFeInput);
		generarArista("Buenos Aires", "Santa Fe");
		
		this.bsAsCABAInput = new JTextField();
		this.bsAsCABAInput.setColumns(10);
		this.bsAsCABAInput.setBounds(135, 246, 31, 20);
		add(this.bsAsCABAInput);
		this.inputs.add(bsAsCABAInput);
		generarArista("Buenos Aires", "CABA");
		
		this.mendozaSanLuisInput = new JTextField();
		this.mendozaSanLuisInput.setColumns(10);
		this.mendozaSanLuisInput.setBounds(364, 277, 31, 20);
		add(this.mendozaSanLuisInput);
		this.inputs.add(mendozaSanLuisInput);
		generarArista("Mendoza", "San Luis");
		
		this.bsAsEntreRiosInput = new JTextField();
		this.bsAsEntreRiosInput.setColumns(10);
		this.bsAsEntreRiosInput.setBounds(135, 277, 31, 20);
		add(this.bsAsEntreRiosInput);
		this.inputs.add(bsAsEntreRiosInput);
		generarArista("Buenos Aires", "Entre Rios");
		
		this.sanLuisCordobaInput = new JTextField();
		this.sanLuisCordobaInput.setColumns(10);
		this.sanLuisCordobaInput.setBounds(364, 308, 31, 20);
		add(this.sanLuisCordobaInput);
		this.inputs.add(sanLuisCordobaInput);
		generarArista("San Luis", "Cordoba");
		
		this.mendozaSanJuanInput = new JTextField();
		this.mendozaSanJuanInput.setColumns(10);
		this.mendozaSanJuanInput.setBounds(135, 308, 31, 20);
		add(this.mendozaSanJuanInput);
		this.inputs.add(mendozaSanJuanInput);
		generarArista("Mendoza", "San Juan");
		
		this.sanLuisLaRiojaInput = new JTextField();
		this.sanLuisLaRiojaInput.setColumns(10);
		this.sanLuisLaRiojaInput.setBounds(364, 339, 31, 20);
		add(this.sanLuisLaRiojaInput);
		this.inputs.add(sanLuisLaRiojaInput);
		generarArista("San Luis", "La Rioja");
		
		this.sanLuisSanJuanInput = new JTextField();
		this.sanLuisSanJuanInput.setColumns(10);
		this.sanLuisSanJuanInput.setBounds(135, 339, 31, 20);
		add(this.sanLuisSanJuanInput);
		this.inputs.add(sanLuisSanJuanInput);
		generarArista("San Luis", "San Juan");
		
		this.cbaStgoDelEsteroInput = new JTextField();
		this.cbaStgoDelEsteroInput.setColumns(10);
		this.cbaStgoDelEsteroInput.setBounds(364, 370, 31, 20);
		add(this.cbaStgoDelEsteroInput);
		this.inputs.add(cbaStgoDelEsteroInput);
		generarArista("Cordoba", "Santiago Del Estero");
		
		this.cordobaSantaFeInput = new JTextField();
		this.cordobaSantaFeInput.setColumns(10);
		this.cordobaSantaFeInput.setBounds(135, 370, 31, 20);
		add(this.cordobaSantaFeInput);
		this.inputs.add(cordobaSantaFeInput);
		generarArista("Cordoba", "Santa Fe");
		
		this.cbaCatamarcaInput = new JTextField();
		this.cbaCatamarcaInput.setColumns(10);
		this.cbaCatamarcaInput.setBounds(364, 401, 31, 20);
		add(this.cbaCatamarcaInput);
		this.inputs.add(cbaCatamarcaInput);
		generarArista("Cordoba", "Catamarca");
		
		this.cbaLaRiojaInput = new JTextField();
		this.cbaLaRiojaInput.setColumns(10);
		this.cbaLaRiojaInput.setBounds(135, 401, 31, 20);
		add(this.cbaLaRiojaInput);
		this.inputs.add(cbaLaRiojaInput);
		generarArista("Cordoba", "La Rioja");
		
		this.santaFeStgoDelEsteroInput = new JTextField();
		this.santaFeStgoDelEsteroInput.setColumns(10);
		this.santaFeStgoDelEsteroInput.setBounds(364, 432, 31, 20);
		add(this.santaFeStgoDelEsteroInput);
		this.inputs.add(santaFeStgoDelEsteroInput);
		generarArista("Santa Fe", "Santiago Del Estero");
		
		this.santaFeEntreRiosInput = new JTextField();
		this.santaFeEntreRiosInput.setColumns(10);
		this.santaFeEntreRiosInput.setBounds(135, 432, 31, 20);
		add(this.santaFeEntreRiosInput);
		this.inputs.add(santaFeEntreRiosInput);
		generarArista("Santa Fe", "Entre Rios");
		
		this.santaFeChacoInput = new JTextField();
		this.santaFeChacoInput.setColumns(10);
		this.santaFeChacoInput.setBounds(364, 463, 31, 20);
		add(this.santaFeChacoInput);
		this.inputs.add(santaFeChacoInput);
		generarArista("Santa Fe", "Chaco");
		
		this.santaFeCorrientesInput = new JTextField();
		this.santaFeCorrientesInput.setColumns(10);
		this.santaFeCorrientesInput.setBounds(135, 463, 31, 20);
		add(this.santaFeCorrientesInput);
		this.inputs.add(santaFeCorrientesInput);
		generarArista("Santa Fe", "Corrientes");
		
		this.sanJuanLaRiojaInput = new JTextField();
		this.sanJuanLaRiojaInput.setColumns(10);
		this.sanJuanLaRiojaInput.setBounds(364, 494, 31, 20);
		add(this.sanJuanLaRiojaInput);
		this.inputs.add(sanJuanLaRiojaInput);
		generarArista("San Juan", "La Rioja");
		
		this.entreRiosCorrientesInput = new JTextField();
		this.entreRiosCorrientesInput.setColumns(10);
		this.entreRiosCorrientesInput.setBounds(135, 494, 31, 20);
		add(this.entreRiosCorrientesInput);
		this.inputs.add(entreRiosCorrientesInput);
		generarArista("Entre Rios", "Corrientes");
		
		this.catamarcaStgoDelEsteroInput = new JTextField();
		this.catamarcaStgoDelEsteroInput.setColumns(10);
		this.catamarcaStgoDelEsteroInput.setBounds(364, 525, 31, 20);
		add(this.catamarcaStgoDelEsteroInput);
		this.inputs.add(catamarcaStgoDelEsteroInput);
		generarArista("Catamarca", "Santiago Del Estero");
		
		this.laRiojaCatamarcaInput = new JTextField();
		this.laRiojaCatamarcaInput.setColumns(10);
		this.laRiojaCatamarcaInput.setBounds(135, 525, 31, 20);
		add(this.laRiojaCatamarcaInput);
		this.inputs.add(laRiojaCatamarcaInput);
		generarArista("La Rioja", "Catamarca");
		
		this.catamarcaSaltaInput = new JTextField();
		this.catamarcaSaltaInput.setColumns(10);
		this.catamarcaSaltaInput.setBounds(364, 556, 31, 20);
		add(this.catamarcaSaltaInput);
		this.inputs.add(catamarcaSaltaInput);
		generarArista("Catamarca", "Salta");
		
		this.catamarcaTucumanInput = new JTextField();
		this.catamarcaTucumanInput.setColumns(10);
		this.catamarcaTucumanInput.setBounds(135, 556, 31, 20);
		add(this.catamarcaTucumanInput);
		this.inputs.add(catamarcaTucumanInput);
		generarArista("Catamarca", "Tucuman");
		
		this.tucumanSaltaInput = new JTextField();
		this.tucumanSaltaInput.setColumns(10);
		this.tucumanSaltaInput.setBounds(364, 587, 31, 20);
		add(this.tucumanSaltaInput);
		this.inputs.add(tucumanSaltaInput);
		generarArista("Tucuman", "Salta");
		
		this.tucumanStgoDelEsteroInput = new JTextField();
		this.tucumanStgoDelEsteroInput.setColumns(10);
		this.tucumanStgoDelEsteroInput.setBounds(135, 587, 31, 20);
		add(this.tucumanStgoDelEsteroInput);
		this.inputs.add(tucumanStgoDelEsteroInput);
		generarArista("Tucuman", "Santiago Del Estero");
		
		this.stgoDelEsteroChacoInput = new JTextField();
		this.stgoDelEsteroChacoInput.setColumns(10);
		this.stgoDelEsteroChacoInput.setBounds(364, 618, 31, 20);
		add(this.stgoDelEsteroChacoInput);
		this.inputs.add(stgoDelEsteroChacoInput);
		generarArista("Santiago Del Estero", "Chaco");
		
		this.stgoDelEsteroSaltaInput = new JTextField();
		this.stgoDelEsteroSaltaInput.setColumns(10);
		this.stgoDelEsteroSaltaInput.setBounds(135, 618, 31, 20);
		add(this.stgoDelEsteroSaltaInput);
		this.inputs.add(stgoDelEsteroSaltaInput);
		generarArista("Santiago Del Estero", "Salta");
		
		this.chacoFormosaInput = new JTextField();
		this.chacoFormosaInput.setColumns(10);
		this.chacoFormosaInput.setBounds(364, 649, 31, 20);
		add(this.chacoFormosaInput);
		this.inputs.add(chacoFormosaInput);
		generarArista("Chaco", "Formosa");
		
		this.chacoSaltaInput = new JTextField();
		this.chacoSaltaInput.setColumns(10);
		this.chacoSaltaInput.setBounds(135, 649, 31, 20);
		add(this.chacoSaltaInput);
		this.inputs.add(chacoSaltaInput);
		generarArista("Chaco", "Salta");
		
		this.corrientesMisionesInput = new JTextField();
		this.corrientesMisionesInput.setColumns(10);
		this.corrientesMisionesInput.setBounds(364, 680, 31, 20);
		add(this.corrientesMisionesInput);
		this.inputs.add(corrientesMisionesInput);
		generarArista("Corrientes", "Misiones");
		
		this.chacoCorrientesInput = new JTextField();
		this.chacoCorrientesInput.setColumns(10);
		this.chacoCorrientesInput.setBounds(135, 680, 31, 20);
		add(this.chacoCorrientesInput);
		this.inputs.add(chacoCorrientesInput);
		generarArista("Chaco", "Corrientes");
		
		this.saltaFormosaInput = new JTextField();
		this.saltaFormosaInput.setColumns(10);
		this.saltaFormosaInput.setBounds(364, 711, 31, 20);
		add(this.saltaFormosaInput);
		this.inputs.add(saltaFormosaInput);
		generarArista("Salta", "Formosa");
		
		this.saltaJujuyInput = new JTextField();
		this.saltaJujuyInput.setColumns(10);
		this.saltaJujuyInput.setBounds(135, 711, 31, 20);
		add(this.saltaJujuyInput);		
		this.inputs.add(saltaJujuyInput);
		generarArista("Salta", "Jujuy");
		
		this.cantRegionesInput = new JTextField();
		this.cantRegionesInput.setColumns(10);
		this.cantRegionesInput.setBounds(315, 930, 31, 20);
		add(this.cantRegionesInput);
	}
	public void inicializarTextFields() {
		
		this.tierraDelFuegoSantaCruzText = new JTextArea();
		this.tierraDelFuegoSantaCruzText.setLineWrap(true);
		this.tierraDelFuegoSantaCruzText.setFont(new Font("Tahoma", Font.PLAIN, 10));
		this.tierraDelFuegoSantaCruzText.setEditable(false);
		this.tierraDelFuegoSantaCruzText.setText("Tierra del Fuego - Santa Cruz");
		this.tierraDelFuegoSantaCruzText.setBounds(10, 28, 115, 30);
		add(this.tierraDelFuegoSantaCruzText);
		
		this.santaCruzChubutText = new JTextArea();
		this.santaCruzChubutText.setText("Santa Cruz - Chubut");
		this.santaCruzChubutText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.santaCruzChubutText.setEditable(false);
		this.santaCruzChubutText.setBounds(239, 32, 115, 20);
		add(this.santaCruzChubutText);
		
		this.chubutRionegroText = new JTextArea();
		this.chubutRionegroText.setText("Chubut - Rio Negro");
		this.chubutRionegroText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.chubutRionegroText.setEditable(false);
		this.chubutRionegroText.setBounds(10, 61, 115, 20);
		add(this.chubutRionegroText);
		
		this.rioNegroNeuquenText = new JTextArea();
		this.rioNegroNeuquenText.setText("Rio Negro - Neuquen");
		this.rioNegroNeuquenText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.rioNegroNeuquenText.setEditable(false);
		this.rioNegroNeuquenText.setBounds(239, 60, 115, 20);
		add(this.rioNegroNeuquenText);
		
		this.rioNegroLaPampaText = new JTextArea();
		this.rioNegroLaPampaText.setText("Rio Negro - La Pampa");
		this.rioNegroLaPampaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.rioNegroLaPampaText.setEditable(false);
		this.rioNegroLaPampaText.setBounds(10, 92, 115, 20);
		add(this.rioNegroLaPampaText);
		
		this.rioNegroMendozaText = new JTextArea();
		this.rioNegroMendozaText.setText("Rio Negro - Mendoza");
		this.rioNegroMendozaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.rioNegroMendozaText.setEditable(false);
		this.rioNegroMendozaText.setBounds(239, 91, 115, 20);
		add(this.rioNegroMendozaText);
		
		this.rioNegroBsAsText = new JTextArea();
		this.rioNegroBsAsText.setText("Rio Negro - Bs. As.");
		this.rioNegroBsAsText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.rioNegroBsAsText.setEditable(false);
		this.rioNegroBsAsText.setBounds(10, 122, 115, 20);
		add(this.rioNegroBsAsText);
		
		this.meuquenLaPampaText = new JTextArea();
		this.meuquenLaPampaText.setText("Neuquen - La Pampa");
		this.meuquenLaPampaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.meuquenLaPampaText.setEditable(false);
		this.meuquenLaPampaText.setBounds(239, 122, 115, 20);
		add(this.meuquenLaPampaText);
		
		this.neuquenMendozaText = new JTextArea();
		this.neuquenMendozaText.setText("Neuquen - Mendoza");
		this.neuquenMendozaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.neuquenMendozaText.setEditable(false);
		this.neuquenMendozaText.setBounds(10, 153, 115, 20);
		add(this.neuquenMendozaText);
		
		this.laPampaBsAsText = new JTextArea();
		this.laPampaBsAsText.setText("La Pampa - Bs. As.");
		this.laPampaBsAsText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.laPampaBsAsText.setEditable(false);
		this.laPampaBsAsText.setBounds(239, 153, 115, 20);
		add(this.laPampaBsAsText);
		
		this.laPampaMendozaText = new JTextArea();
		this.laPampaMendozaText.setText("La Pampa - Mendoza");
		this.laPampaMendozaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.laPampaMendozaText.setEditable(false);
		this.laPampaMendozaText.setBounds(10, 184, 115, 20);
		add(this.laPampaMendozaText);
		
		this.laPampaSanLuisText = new JTextArea();
		this.laPampaSanLuisText.setText("La Pampa - San Luis");
		this.laPampaSanLuisText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.laPampaSanLuisText.setEditable(false);
		this.laPampaSanLuisText.setBounds(239, 184, 115, 20);
		add(this.laPampaSanLuisText);
		
		this.laPampaCordobaText = new JTextArea();
		this.laPampaCordobaText.setText("La Pampa - Cordoba");
		this.laPampaCordobaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.laPampaCordobaText.setEditable(false);
		this.laPampaCordobaText.setBounds(10, 215, 115, 20);
		add(this.laPampaCordobaText);
		
		this.bsAsCordobaText = new JTextArea();
		this.bsAsCordobaText.setText("Bs. As. - Cordoba");
		this.bsAsCordobaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.bsAsCordobaText.setEditable(false);
		this.bsAsCordobaText.setBounds(239, 215, 115, 20);
		add(this.bsAsCordobaText);
		
		this.bsAsCABAText = new JTextArea();
		this.bsAsCABAText.setText("Bs. As. - C.A.B.A.");
		this.bsAsCABAText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.bsAsCABAText.setEditable(false);
		this.bsAsCABAText.setBounds(10, 246, 115, 20);
		add(this.bsAsCABAText);
		
		this.bsAsSantaFeText = new JTextArea();
		this.bsAsSantaFeText.setText("Bs. As. - Santa Fe");
		this.bsAsSantaFeText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.bsAsSantaFeText.setEditable(false);
		this.bsAsSantaFeText.setBounds(239, 246, 115, 20);
		add(this.bsAsSantaFeText);
		
		this.bsAsEntreRiosText = new JTextArea();
		this.bsAsEntreRiosText.setText("Bs. As. - Entre Rios");
		this.bsAsEntreRiosText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.bsAsEntreRiosText.setEditable(false);
		this.bsAsEntreRiosText.setBounds(10, 277, 115, 20);
		add(bsAsEntreRiosText);
		
		this.mendozaSanLuisText = new JTextArea();
		this.mendozaSanLuisText.setText("Mendoza - San Luis");
		this.mendozaSanLuisText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.mendozaSanLuisText.setEditable(false);
		this.mendozaSanLuisText.setBounds(239, 277, 115, 20);
		add(this.mendozaSanLuisText);
		
		this.mendozaSanJuanText = new JTextArea();
		this.mendozaSanJuanText.setText("Mendoza - San Juan");
		this.mendozaSanJuanText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.mendozaSanJuanText.setEditable(false);
		this.mendozaSanJuanText.setBounds(10, 308, 115, 20);
		add(this.mendozaSanJuanText);
		
		this.sanLuisCordobaText = new JTextArea();
		this.sanLuisCordobaText.setText("San Luis - Cordoba");
		this.sanLuisCordobaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.sanLuisCordobaText.setEditable(false);
		this.sanLuisCordobaText.setBounds(239, 308, 115, 20);
		add(this.sanLuisCordobaText);
		
		this.sanLuisSanJuanText = new JTextArea();
		this.sanLuisSanJuanText.setText("San Luis - San Juan");
		this.sanLuisSanJuanText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.sanLuisSanJuanText.setEditable(false);
		this.sanLuisSanJuanText.setBounds(10, 339, 115, 20);
		add(this.sanLuisSanJuanText);
		
		this.sanLuisLaRiojaText = new JTextArea();
		this.sanLuisLaRiojaText.setText("San Luis - La Rioja");
		this.sanLuisLaRiojaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.sanLuisLaRiojaText.setEditable(false);
		this.sanLuisLaRiojaText.setBounds(239, 339, 115, 20);
		add(this.sanLuisLaRiojaText);
		
		this.cordobaSantaFeText = new JTextArea();
		this.cordobaSantaFeText.setText("Cordoba - Santa Fe");
		this.cordobaSantaFeText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.cordobaSantaFeText.setEditable(false);
		this.cordobaSantaFeText.setBounds(10, 370, 115, 20);
		add(this.cordobaSantaFeText);
		
		this.cbaStgodelEsteroText = new JTextArea();
		this.cbaStgodelEsteroText.setText("Cba - Stgo. del Estero");
		this.cbaStgodelEsteroText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.cbaStgodelEsteroText.setEditable(false);
		this.cbaStgodelEsteroText.setBounds(239, 371, 115, 20);
		add(this.cbaStgodelEsteroText);
		
		this.cordobaLaRiojaText = new JTextArea();
		this.cordobaLaRiojaText.setText("Cordoba - La Rioja");
		this.cordobaLaRiojaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.cordobaLaRiojaText.setEditable(false);
		this.cordobaLaRiojaText.setBounds(10, 401, 115, 20);
		add(this.cordobaLaRiojaText);
		
		this.cordobaCatamarcaText = new JTextArea();
		this.cordobaCatamarcaText.setText("Cordoba - Catamarca");
		this.cordobaCatamarcaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.cordobaCatamarcaText.setEditable(false);
		this.cordobaCatamarcaText.setBounds(239, 401, 115, 20);
		add(this.cordobaCatamarcaText);
		
		this.santaFeEntreRiosText = new JTextArea();
		this.santaFeEntreRiosText.setText("Santa Fe - Entre Rios");
		this.santaFeEntreRiosText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.santaFeEntreRiosText.setEditable(false);
		this.santaFeEntreRiosText.setBounds(10, 432, 115, 20);
		add(this.santaFeEntreRiosText);
		
		this.santaFeStgodelEsteroText = new JTextArea();
		this.santaFeStgodelEsteroText.setText("Santa Fe - Stgo. del Estero");
		this.santaFeStgodelEsteroText.setFont(new Font("Tahoma", Font.PLAIN, 9));
		this.santaFeStgodelEsteroText.setEditable(false);
		this.santaFeStgodelEsteroText.setBounds(239, 432, 115, 20);
		add(this.santaFeStgodelEsteroText);
		
		this.santaFeCorrientesText = new JTextArea();
		this.santaFeCorrientesText.setText("Santa Fe - Corrientes");
		this.santaFeCorrientesText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.santaFeCorrientesText.setEditable(false);
		this.santaFeCorrientesText.setBounds(10, 463, 115, 20);
		add(this.santaFeCorrientesText);
		
		this.santaFeChacoText = new JTextArea();
		this.santaFeChacoText.setText("Santa Fe - Chaco");
		this.santaFeChacoText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.santaFeChacoText.setEditable(false);
		this.santaFeChacoText.setBounds(239, 463, 115, 20);
		add(this.santaFeChacoText);
		
		this.entreRiosCorrientesText = new JTextArea();
		this.entreRiosCorrientesText.setText("Entre Rios - Corrientes");
		this.entreRiosCorrientesText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.entreRiosCorrientesText.setEditable(false);
		this.entreRiosCorrientesText.setBounds(10, 494, 115, 20);
		add(this.entreRiosCorrientesText);
		
		this.sanJuanLaRiojaText = new JTextArea();
		this.sanJuanLaRiojaText.setText("San Juan - La Rioja");
		this.sanJuanLaRiojaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.sanJuanLaRiojaText.setEditable(false);
		this.sanJuanLaRiojaText.setBounds(239, 494, 115, 20);
		add(this.sanJuanLaRiojaText);
		
		this.laRiojaCatamarcaText = new JTextArea();
		this.laRiojaCatamarcaText.setText("La Rioja - Catamarca");
		this.laRiojaCatamarcaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.laRiojaCatamarcaText.setEditable(false);
		this.laRiojaCatamarcaText.setBounds(10, 525, 115, 20);
		add(this.laRiojaCatamarcaText);
		
		this.catamarcaStgodelEsteroText = new JTextArea();
		this.catamarcaStgodelEsteroText.setText("Catamarca - Stgo. del Estero");
		this.catamarcaStgodelEsteroText.setFont(new Font("Tahoma", Font.PLAIN, 9));
		this.catamarcaStgodelEsteroText.setEditable(false);
		this.catamarcaStgodelEsteroText.setBounds(239, 525, 125, 20);
		add(this.catamarcaStgodelEsteroText);
		
		this.catamarcaTucumanText = new JTextArea();
		this.catamarcaTucumanText.setText("Catamarca - Tucuman");
		this.catamarcaTucumanText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.catamarcaTucumanText.setEditable(false);
		this.catamarcaTucumanText.setBounds(10, 556, 115, 20);
		add(this.catamarcaTucumanText);
		
		this.catamarcaSaltaText = new JTextArea();
		this.catamarcaSaltaText.setText("Catamarca - Salta");
		this.catamarcaSaltaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.catamarcaSaltaText.setEditable(false);
		this.catamarcaSaltaText.setBounds(239, 556, 115, 20);
		add(this.catamarcaSaltaText);
		
		this.tucumanStgodelEsteroText = new JTextArea();
		this.tucumanStgodelEsteroText.setText("Tucuman - Stgo. del Estero");
		this.tucumanStgodelEsteroText.setFont(new Font("Tahoma", Font.PLAIN, 9));
		this.tucumanStgodelEsteroText.setEditable(false);
		this.tucumanStgodelEsteroText.setBounds(10, 587, 115, 20);
		add(this.tucumanStgodelEsteroText);
		
		this.tucumanSaltaText = new JTextArea();
		this.tucumanSaltaText.setText("Tucuman - Salta");
		this.tucumanSaltaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.tucumanSaltaText.setEditable(false);
		this.tucumanSaltaText.setBounds(239, 587, 115, 20);
		add(this.tucumanSaltaText);
		
		this.stgodelEsteroSaltaText = new JTextArea();
		this.stgodelEsteroSaltaText.setText("Stgo. del Estero - Salta");
		this.stgodelEsteroSaltaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.stgodelEsteroSaltaText.setEditable(false);
		this.stgodelEsteroSaltaText.setBounds(10, 618, 115, 20);
		add(this.stgodelEsteroSaltaText);
		
		this.stgodelEsteroChacoText = new JTextArea();
		this.stgodelEsteroChacoText.setText("Stgo. del Estero - Chaco");
		this.stgodelEsteroChacoText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.stgodelEsteroChacoText.setEditable(false);
		this.stgodelEsteroChacoText.setBounds(239, 618, 125, 20);
		add(this.stgodelEsteroChacoText);
		
		this.chacoSaltaText = new JTextArea();
		this.chacoSaltaText.setText("Chaco - Salta");
		this.chacoSaltaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.chacoSaltaText.setEditable(false);
		this.chacoSaltaText.setBounds(10, 649, 115, 20);
		add(this.chacoSaltaText);
		
		this.chacoFormosaText = new JTextArea();
		this.chacoFormosaText.setText("Chaco - Formosa");
		this.chacoFormosaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.chacoFormosaText.setEditable(false);
		this.chacoFormosaText.setBounds(239, 649, 115, 20);
		add(this.chacoFormosaText);
		
		this.chacoCorrientesText = new JTextArea();
		this.chacoCorrientesText.setText("Chaco - Corrientes");
		this.chacoCorrientesText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.chacoCorrientesText.setEditable(false);
		this.chacoCorrientesText.setBounds(10, 680, 115, 20);
		add(this.chacoCorrientesText);
		
		this.corrientesMisionesText = new JTextArea();
		this.corrientesMisionesText.setText("Corrientes - Misiones");
		this.corrientesMisionesText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.corrientesMisionesText.setEditable(false);
		this.corrientesMisionesText.setBounds(239, 680, 115, 20);
		add(this.corrientesMisionesText);
		
		this.saltaJujuyText = new JTextArea();
		this.saltaJujuyText.setText("Salta - Jujuy");
		this.saltaJujuyText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.saltaJujuyText.setEditable(false);
		this.saltaJujuyText.setBounds(10, 711, 115, 20);
		add(this.saltaJujuyText);
		
		this.saltaFormosaText = new JTextArea();
		this.saltaFormosaText.setText("Salta - Formosa");
		this.saltaFormosaText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.saltaFormosaText.setEditable(false);
		this.saltaFormosaText.setBounds(239, 711, 115, 20);
		add(this.saltaFormosaText);
		
		this.regionesASepararText = new JTextArea();
		this.regionesASepararText.setText("Regiones a separar");
		this.regionesASepararText.setFont(new Font("Tahoma", Font.PLAIN, 11));
		this.regionesASepararText.setEditable(false);
		this.regionesASepararText.setBounds(109, 849, 115, 20);
		add(this.regionesASepararText);
		
	}
	public void inicializarTitulo() {
		
		this.titulo = new JEditorPane();
		this.titulo.setEditable(false);
		this.titulo.setText("Introdusca los pesos de las aristas");
		this.titulo.setBounds(111, 0, 243, 22);
		add(this.titulo);
		
	}
	public void inicializarBotones() {
		
		this.calcularRegionesButton = new JButton("Calcular");
		this.calcularRegionesButton.setBounds(200, 927, 90, 23);//.setBounds(175, 927, 90, 23);
		add(this.calcularRegionesButton);	
		
		this.mostrarGrafoCompletoButton = new JButton("Mapa Completo");
		this.mostrarGrafoCompletoButton.setBounds(25, 927, 150, 23);
		add(this.mostrarGrafoCompletoButton);	
		
	}

	public void generarArista(String nombre1, String nombre2) {
		boolean v1 = true;
		boolean v2 = true;
		Vertice vertice1= null;
		Vertice vertice2 = null;		
			for (Vertice v : this.mapa.getVertices()) {				
				if (v.getNombre().equals(nombre1)) {
					vertice1 = new Vertice(v.getCoordenada(), v.getNombre());
					v1 = false;
				}
				if (v.getNombre().equals(nombre2)) {
					vertice2 = new Vertice(v.getCoordenada(), v.getNombre());
					v2 = false;
				}
			}		
		this.mapa.getAristas().add(new Arista(0,vertice1,vertice2));
	}
	
	public JButton getCalcularRegionesButton() {
		return this.calcularRegionesButton;
	}
	public JButton getMostrarGrafoCompletoButton() {
		return this.mostrarGrafoCompletoButton;
	}
	public JTextField getCantRegionesInput() {
		return this.cantRegionesInput;
	}
	public ArrayList<JTextField> getInputsPesos(){
		return this.inputs;
	}
	public ArrayList<JTextArea> getTexts(){
		return this.texts;
	}
}


