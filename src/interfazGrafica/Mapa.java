package interfazGrafica;

import java.awt.Color;
import java.awt.GridLayout;
import java.lang.ModuleLayer.Controller;
import java.util.ArrayList;

import org.openstreetmap.gui.jmapviewer.Coordinate;
import org.openstreetmap.gui.jmapviewer.JMapViewer;
import org.openstreetmap.gui.jmapviewer.MapMarkerDot;
import org.openstreetmap.gui.jmapviewer.MapPolygonImpl;
import org.openstreetmap.gui.jmapviewer.interfaces.MapMarker;
import org.openstreetmap.gui.jmapviewer.interfaces.MapPolygon;

@SuppressWarnings("serial")
public class Mapa extends JMapViewer{
	
	private Coordinate coordArgentina;
	private ArrayList<Vertice> vertices;
	private ArrayList<Arista> aristas;
	private Vertice verticeBsAs;
	private Vertice verticeBsAsCapital;
	private Vertice verticeEntreRios;
	private Vertice verticeCorrientes;
	private Vertice verticeMisiones;
	private Vertice verticeChaco;
	private Vertice verticeFormosa;
	private Vertice verticeSantiago;
	private Vertice verticeJujuy;
	private Vertice verticeSalta;
	private Vertice verticeCordoba;
	private Vertice verticeTucuman;
	private Vertice verticeCatamarca;
	private Vertice verticeSantaFe;
	private Vertice verticeLaRioja;
	private Vertice verticeSJuan;
	private Vertice verticeMendoza;
	private Vertice verticeSLuis;
	private Vertice verticeLaPampa;
	private Vertice verticeRNegro;
	private Vertice verticeNeuquen;
	private Vertice verticeChubut;
	private Vertice verticeSCruz;
	private Vertice verticeTDFuego;

	
	public Mapa(){
		this.coordArgentina = new Coordinate(-40.416097,-63.616672);
	    this.vertices= new ArrayList<Vertice>();
	    this.aristas = new ArrayList<Arista>();
		configurarMapa();	
		inicializarVertices();
		colocarVertices();
		
	}
	
	public void configurarMapa() {		
		
		setDisplayPosition(this.coordArgentina, 5);			
		setLayout(new GridLayout(0, 1, 0, 0));
		setBounds(461,0,573,1011);		
		setZoomControlsVisible(false);
		
	}
	public void inicializarVertices() {
		
		this.verticeBsAs = new Vertice(new Coordinate(-36.2, -60.0), "Buenos Aires");
		vertices.add(this.verticeBsAs);
		
		this.verticeBsAsCapital =new Vertice( new Coordinate(-34.5, -58.8), "CABA");
		vertices.add(this.verticeBsAsCapital);
		
		this.verticeEntreRios = new Vertice( new Coordinate(-32.0, -59.1), "Entre Rios");
		vertices.add(this.verticeEntreRios);
		
		this.verticeCorrientes =new Vertice(  new Coordinate(-28.5, -58.0),"Corrientes");
		vertices.add(this.verticeCorrientes);
		
		this.verticeMisiones = new Vertice( new Coordinate(-26.8, -54.5), "Misiones");
		vertices.add(this.verticeMisiones);
		
		this.verticeChaco = new Vertice( new Coordinate(-26.8, -60.5), "Chaco");
		vertices.add(this.verticeChaco);
		
		this.verticeFormosa =new Vertice(  new Coordinate(-25.3, -60.0), "Formosa");
		vertices.add(this.verticeFormosa);
		
		this.verticeSantiago = new Vertice( new Coordinate(-27.5, -63.5), "Santiago Del Estero");
		vertices.add(this.verticeSantiago);
		
		this.verticeJujuy =new Vertice(  new Coordinate(-23.0, -66.0), "Jujuy");
		vertices.add(this.verticeJujuy);
		
		this.verticeSalta =new Vertice(  new Coordinate(-25.3, -65.0),"Salta");
		vertices.add(this.verticeSalta);
		
		this.verticeCordoba =new Vertice(  new Coordinate(-32.0, -63.8),"Cordoba");
		vertices.add(this.verticeCordoba);
		
		this.verticeTucuman = new Vertice( new Coordinate(-26.8, -65.35), "Tucuman");
		vertices.add(this.verticeTucuman);
		
		this.verticeCatamarca = new Vertice( new Coordinate(-27.2, -67.2), "Catamarca");
		vertices.add(this.verticeCatamarca);
		
		this.verticeSantaFe = new Vertice( new Coordinate(-30.7,-60.5), "Santa Fe");
		vertices.add(this.verticeSantaFe);
		
		this.verticeLaRioja =new Vertice(  new Coordinate(-29.4, -67.2), "La Rioja");
		vertices.add(this.verticeLaRioja);
		
		this.verticeSJuan = new Vertice( new Coordinate(-30.7, -68.8), "San Juan");
		vertices.add(this.verticeSJuan);
		
		this.verticeMendoza =new Vertice( new Coordinate(-34.1, -68.5), "Mendoza");
		vertices.add(this.verticeMendoza);
		
		this.verticeSLuis =new Vertice(  new Coordinate(-33.5, -65.9), "San Luis");
		vertices.add(this.verticeSLuis);
		
		this.verticeLaPampa = new Vertice( new Coordinate(-37.0, -65.5), "La Pampa");
		vertices.add(this.verticeLaPampa);
		
		this.verticeRNegro = new Vertice( new Coordinate(-40.0, -66.5), "Rio Negro");
		vertices.add(this.verticeRNegro);
		
		this.verticeNeuquen = new Vertice( new Coordinate(-38.5, -70.0), "Neuquen");
		vertices.add(this.verticeNeuquen);
		
		this.verticeChubut = new Vertice( new Coordinate(-43.8, -68.5), "Chubut");
		vertices.add(this.verticeChubut);
		
		this.verticeSCruz = new Vertice( new Coordinate(-48.8, -70.0), "Santa Cruz");
		vertices.add(this.verticeSCruz);
		
		this.verticeTDFuego = new Vertice( new Coordinate(-54.0, -68.0), "Tierra Del Fuego");
		vertices.add(this.verticeTDFuego);
		
	}
	public void colocarVertices() {
		
		for(Vertice v : this.vertices) {
			MapMarker vertice= new MapMarkerDot(v.getNombre(), v.getCoordenada());
			vertice.getStyle().setBackColor(Color.black);
			addMapMarker(vertice);			
		}
	}
	public void colocarAristas() {

		for (Arista a : aristas) {
			MapPolygon arista= new MapPolygonImpl(a.getVertice1().getCoordenada(),
					                                                       a.getVertice2().getCoordenada(), 
					                                                       a.getVertice1().getCoordenada());
			addMapPolygon(arista);		
		}
	}
	
	public Vertice getVertice(String nombreProvincia) {
		
		for(Vertice vertice : this.vertices) {
			if(vertice.getNombre().equals(nombreProvincia))
				return vertice;
		}
		
		throw new IllegalArgumentException("Provincia inválida");
	}

	
	public ArrayList<Vertice> getVertices() {
		return vertices;
	}

	public void setVertices(ArrayList<Vertice> vertices) {
		this.vertices = vertices;
	}

	public ArrayList<Arista> getAristas() {
		return aristas;
	}

	public void setAristas(ArrayList<Arista> aristas) {
		this.aristas = aristas;
	}

	@Override
	public String toString() {
		return "Mapa [aristas=" + aristas + "]";
	}

	
}
